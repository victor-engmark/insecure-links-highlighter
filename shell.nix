let
  pkgs = import (builtins.fetchTarball {
    name = "nixos-unstable-2025-02-27";
    url = "https://github.com/nixos/nixpkgs/archive/5135c59491985879812717f4c9fea69604e7f26f.tar.gz";
    sha256 = "09qy7zv80bkd9ighsw0bdxjq70dw3qjnyvg7il1fycrsgs5x1gan";
  }) { };
  inherit (pkgs) jdk;
  checkstylePackage = pkgs.checkstyle;
  checkstyleSource = pkgs.fetchFromGitHub {
    owner = "checkstyle";
    repo = "checkstyle";
    rev = "checkstyle-${checkstylePackage.version}";
    hash = "sha256-5C8bZIQ+dwDDSrFsVGBVI5HecHtSHEV/91YEZHHEEkw=";
  };
in
pkgs.mkShell {
  packages = [
    pkgs.bashInteractive
    pkgs.cacert
    pkgs.check-jsonschema
    checkstylePackage
    pkgs.deadnix
    pkgs.firefox-devedition-bin
    pkgs.geckodriver
    pkgs.gitFull
    pkgs.gitlint
    (pkgs.gradle.override {
      java = jdk;
    })
    pkgs.imagemagick
    jdk
    pkgs.jq
    pkgs.nixfmt-rfc-style
    pkgs.nodejs
    pkgs.oxipng
    pkgs.pre-commit
    pkgs.shellcheck
    pkgs.shfmt
    pkgs.statix
    pkgs.yq-go
    pkgs.zip
  ];
  env = {
    LOCALE_ARCHIVE = "${pkgs.glibcLocales}/lib/locale/locale-archive";
  };
  shellHook = ''
    ln --force --no-target-directory --symbolic "${jdk}/lib/openjdk" openjdk
    ln --force --no-target-directory --symbolic "${checkstyleSource}/src/main/resources/google_checks.xml" google_checks.xml
    export PATH="''${PWD}/node_modules/.bin:$PATH"
  '';
}
