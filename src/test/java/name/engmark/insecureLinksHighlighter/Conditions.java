package name.engmark.insecureLinksHighlighter;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.ui.ExpectedCondition;

public final class Conditions {
    private Conditions() {
    }

    public static ExpectedCondition<Boolean> elementsWithEventHandlersAreInsecureIsSet() {
        return driver -> (Boolean) ((JavascriptExecutor) driver).executeScript("return browser.storage.local.get(defaultOptions).then(options => options.elementsWithEventHandlersAreInsecure)");
    }
}
