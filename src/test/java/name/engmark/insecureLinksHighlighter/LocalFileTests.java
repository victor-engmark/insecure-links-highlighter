package name.engmark.insecureLinksHighlighter;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class LocalFileTests extends TestCase {
    @Before
    public void loadPage() {
        getIndex();
    }

    @Test
    public void shouldHighlightHttpUrl() {
        assertLinkIsHighlighted("http");
    }

    @Test
    public void shouldNotHighlightHttpsUrl() {
        assertLinkIsNotHighlighted("https");
    }

    @Test
    public void shouldHighlightHttpUrlWithCustomCss() {
        assertLinkIsHighlighted("styled");
    }

    @Test
    public void shouldHighlightHttpUrlWhichIsInjectedIntoDom() {
        assertLinkIsHighlighted("injected");
    }

    @Test
    public void shouldHighlightUrlWhichChangesToHttpWhenMousedOver() {
        final String linkText = "modify on mouse over";

        final WebElement link = getDriver().findElement(By.linkText(linkText));
        final Actions actions = new Actions(getDriver());
        actions.moveToElement(link).build().perform();

        assertLinkIsHighlighted(linkText);
    }

    @Test
    public void shouldNotHighlightUrlWithOnmousedownHandlerByDefault() {
        assertLinkIsNotHighlighted("modify on mouse down");
    }

    @Test
    public void shouldHighlightHttpUrlInIframe() {
        getDriver().switchTo().frame("iframe");
        assertLinkIsHighlighted("http");
    }
}
